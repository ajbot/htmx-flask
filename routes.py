from flask import Flask, render_template, request
from datetime import datetime
import json, requests


app = Flask(__name__) 
app.config.from_object(__name__) 

serverPutCount=1

@app.route('/') 
def hello(): 
   return render_template("index.html")

@app.route('/htmx', methods=['GET','POST','DELETE', 'PUT']) 
def htmx():
   return ("",200)

@app.route("/weather", methods=["GET"])
def weatherHtmx():
   zip=request.args.get('zip')
   try:
     r = requests.get('http://api.openweathermap.org/data/2.5/weather?zip=%s,us&appid=<APIKEY>' % zip)
     data=r.json()
     weather = data['weather'][0]['description']
     return weather
   except:
     return "no weather found... must be on the moon"
   
@app.route("/time", methods=["GET"])
def newsHtmx():
   now = datetime.now().time()
   return "The current time is %s" % now