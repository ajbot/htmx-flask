# htmx-flask

Testing the HTMX library with a Flask backend. 

To run,
```bash 
pipenv install
pipenv run python web.py
```

Browse on http://localhost:5000 

[View the related blog post](https://blog.ajbothe.com/notes-on-htmx) 